### 霍亨斯陶芬家族的起源(1)

斯陶芬家族（The Staufer）

弗赖辛的奥托为了解释诸侯们一致选择腓特烈一世·巴巴罗萨作为国王的原因，曾这样写道:

> 到目前为止，在罗马的世界里，高卢和日耳曼的边境内一直有两个著名的家族，一个是魏布林根的亨利（Henrys of Waiblingen），还有一个是阿尔特多夫的韦尔夫（Welfs of Altdorf）。一个世代为王族，另一个则是大公爵。这两个家族都渴望建功立业，并且彼此素有嫌隙，经常扰乱国家的安宁。但遵照上帝的旨意（正如人们所相信的那样），为了他的子民的和平，腓特烈二世公爵——即腓特烈一世皇帝的父亲，作为其中一个家族的后裔，迎娶了另一个家族的成员为妻，即巴伐利亚公爵黑色的亨利（Henry the Black）之女，并育有现在作为统治者的腓特烈。 于是诸侯们考虑到这位腓特烈作为这两个家族共同的后裔，将会像一块基石一般把他们联合起来，便决定选他成为王国的领袖。

在这篇著名的选段中，弗赖辛的奥托将他的侄子与萨利安王朝（1024-1125年统治德意志王国及神圣罗马帝国）联系在一起，毕竟腓特烈的外祖母阿格妮丝便来自萨利安家族，她是亨利四世的女儿，亨利五世的姐妹。如果腓特烈仔细地阅读过奥托的编年史，他便会知道自己作为“魏布林根的亨利”【代指王族后裔】，是奥托大帝（Otto the Great, r.936-973）的后裔，也是查理曼大帝古老辉煌的加洛林家族的后裔，甚至可以追溯到法兰克国王克洛维一世（Clovis I, r.481-511）。腓特烈曾有一次在一份宪章中提及查理曼大帝是他的祖先。关于斯陶芬与韦尔夫的敌意一直是德国历史在中世纪盛期时的主题，而奥托的这段话长期以来便被看作是解释这一矛盾的金科玉律。 这也论证了12世纪前德意志地区的贵族是依赖血缘、亲属关系维系的，并且这些贵族都很强调其父系血统，像腓特烈这样的名字时常会在历代领主中重复出现。

事实上在12世纪，母系亲属的重要性甚至比父系亲属更重要，外甥与舅舅之间的关系往往特别密切。例如在12世纪60年代，腓特烈在蒂宾根（Tübingen）的争夺中与他的舅父韦尔夫六世以及自己的表兄弟韦尔夫七世一同对抗自己父系的堂兄弟施瓦本公爵腓特烈四世。国王与贵族偏爱或强调自己的哪一个亲戚与祖先取决于其政治背景以及传承意义，就像伊丽莎白二世作为英国女王可能会声称自己是阿尔弗雷德大帝或是征服者威廉的后裔，而不会说自己是查理曼大帝或腓特烈-巴巴罗萨的后裔一样。腓特烈之所以会被选为国王，正是因为他和腓特烈四世不同，他既是一个韦尔夫，又是一个斯陶芬。在很大程度上，这些血统世系是由像弗赖辛的奥托这样学识渊博的教士与僧侣用想象力构造的，奥托声称他的侄子是魏布林根的亨利中的一员，而腓特烈却从未在他的任何宪章或是诏书中这样称呼过自己。虽然奥托也是阿格妮丝的儿子，他与他同母异父的兄弟们一样可以算得上是萨利安王朝的后裔，但他却在编年史中将自己的父系血统追溯到了一位统治班贝格的名为阿达尔伯特的伯爵，这人在906年6月15日被处死。奥托认为作为奥地利侯爵——后来晋升为公爵，统治奥地利长达270年（976-1246）的巴本堡家族也是这位阿达尔贝特伯爵的后裔。但巴本堡家族是否真的是他的后裔，以及从何种角度追溯是他的后裔，这一点学者们并没有达成共识。

事实上腓特烈的父系“斯陶芬”的血统与皇帝并没有什么关系，源自他外祖母的萨利安王室血统才是最重要的。 公证员只有一次在巴巴罗萨的宪章中提到过施瓦本公爵腓特烈一世（r. 1079-1105），但并没有明说他是皇帝的祖父，与之相反的是，他们却明称亨利四世是巴巴罗萨的外曾祖父。弗赖辛的奥托从未在编年史中提过他母亲的第一任丈夫，但在《契约（the Deeds）》一书中提及了腓特烈公爵1079年与阿格妮丝的婚姻，以及他在施瓦本地区的封建领地，他记载倒：“腓特烈的血统可以追溯到施瓦本最尊贵的伯爵那里”，然而也没有明说他的名字。斯塔沃洛的维巴尔德院长（Abbot Wibald of Stavelot）在他的书信集中收录了当代唯一的一本斯陶芬家谱，他利用这本家谱来证明巴巴罗萨与他的第一任妻子福堡的阿德拉（Adela of Vohburg）是近亲婚姻。维巴尔德写道：

> 这两个同父同母的孩子：腓特烈生下了比伦的腓特烈（Frederick of Büren），比伦的腓特烈生下了建立斯陶芬家族的腓特烈一世公爵。腓特烈公爵与国王亨利四世的女儿成婚并生下了腓特烈二世公爵，腓特烈二世公爵之后又生育了现在的国王腓特烈。
贝莎生育了维林根的贝泽林（Bezelin of Villingen），后者与卡林西亚公爵贝特霍尔德一世（Duke Berthold I of Carinthia）成婚，生下了贝特霍尔德。贝特霍尔德与贝尔德（Beard）生下了柳特嘉德（Liutgard），柳特嘉德生下了福堡侯爵迪博尔德三世（Margrave Diepold III of Vohburg），而他的女儿便是阿德拉。

由于没有其他现存的关于比伦的腓特烈的记载，谱系学者在施瓦本地区找到了两个名为腓特烈的“尊贵的伯爵”，大概他们便是斯陶芬家族出的第一个公爵的父亲和祖父。维巴尔德家谱中提到的两位腓特烈，最有可能是施瓦本的宫相（count-palatine）腓特烈以及腓特烈伯爵。他们在1053年曾被记载出现在施瓦本北部。（腓特烈一世公爵的兄弟路易也担任过宫相）。比伦是一个常见的地名，不过通常被认为是指韦申博伊伦（Wäschenbeuern），位于雷姆斯河谷（Rems valley），即内卡（Neckar）河的支流与斯陶芬家族的修道院之间，而斯陶芬家族的城堡就在其南部。这三个地方都位于多瑙河畔的乌尔姆以北。

比伦的腓特烈娶了达格斯堡-埃吉塞姆的希尔德加德（Hildegard of Dagsburg-Egisheim, d. 1094/95），后者是伟大的宗教改革家教宗利奥九世（r. 1049-1054）的侄女。这样显赫的婚姻表示比伦的腓特烈也是个身居高位的人物，1087年至1094年间的某个时刻，腓特烈一世公爵和他的兄弟们：斯特拉斯堡主教奥托（当时唯一在教会中担任要职的斯陶芬家族成员），还有康拉德拜访了位于西班牙圣地亚哥·德·孔波斯特拉朝圣路（Santiago de Compostela，也叫圣雅各之路）上的本笃会修道院——孔克的圣斐德斯修道院（Abbatiale Sainte-Foy de Conques）。 他们和他们的母亲以及其他的兄弟姐妹深受触动，并将在1094年于阿尔萨斯的塞勒斯塔（Sélestat）建造的教堂用于供奉圣斐德斯。这座教堂也是神圣罗马帝国唯一一座位于法国境内的修道院。希尔德加德被安葬于此，腓特烈一世公爵指定今后承袭施瓦本公爵的斯陶芬家族成员将成为这座修道院的保护人（advocatus）。巴巴罗萨在1153年重新确认了这一安排，并且在1162年击败米兰后为塞勒斯塔的圣斐德斯教堂又添了一扇彩色玻璃窗以纪念他的胜利。根据16世纪阿尔萨斯地区人文主义学者的碑文记载，巴巴罗萨似乎对自己斯陶芬家族的祖先知之甚少，或许是他不愿记起第一个斯陶芬家族的公爵其实出身卑微，这一点很是令人震惊。
