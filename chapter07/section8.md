### 巴巴罗萨与帝国内政（8）

#### Beatrice of Burgundy 勃艮第的贝阿特丽丝

1153年2月，巴巴罗萨大约是在贝桑松逗留期间遇见了贝阿特丽丝。她是已故的勃艮第公爵雷纳德三世（1148年去世）和上洛林公爵西蒙一世的女儿——现任上洛林公爵马蒂亚斯的妹妹阿嘉特唯一的孩子。由于贝阿特丽丝的叔叔兼监护人马孔伯爵威廉四世（Count William IV of Mâcon）实际占有着她的领地，而且也不清楚他和他的儿子们是否会自愿交出勃艮第，巴巴罗萨大约在1153年并没有考虑过与勃艮第联姻；他还在同年9月派遣了一个使节前往君士坦丁堡，希望得到一个希腊人公主作为自己的新娘并延续康拉德三世与拜占庭的同盟。哈维尔贝格的安塞姆在1154年第二次去君士坦丁堡谈判，但在1155年春他回来的时候并没有带回任何一个拜占庭的公主；不过巴巴罗萨在1155年8月又从安科纳派遣维巴尔德去执行同样的任务。1155年9月，马孔伯爵威廉的死改变了局势，马蒂亚斯估计也提醒自己的妻弟【这人娶了巴巴罗萨的姐姐】他的侄女可能会派上用场——1156年1月，马蒂亚斯在斯特拉斯堡会见了巴巴罗萨。1156年6月9日，特里尔大主教希林在沃尔姆斯为贝阿特丽丝加冕为王后。巴巴罗萨在6月10日至17日间于维尔茨堡举行的一次集会上和她举行了婚礼，据弗莱辛的奥托记载，许多诸侯都参加了这次集会。那时她才大约12岁，而巴巴罗萨则已经33岁。他们年龄上的巨大差距以及她在1164至1179年多次怀孕，或许可以解释为什么贝阿特丽丝不像洛塔尔三世的皇后丽琴莎或者她的同辈人阿基坦的埃莉诺那样在政治上扮演着重要角色，她很少参与政治。

贝阿特丽丝在1156年时的部分影响力可能是战略或者军事上的。奥托在记载巴巴罗萨与贝阿特丽丝的婚姻的时候，还提供了一小部分关于勃艮第的简史，从贝阿特丽丝的父亲和扎林根的康拉德如何从1127年到现在为统治勃艮第而战开始。奥托只字未提巴巴罗萨与贝特霍尔德四世于1152年签订的未执行的协议，但他表示，皇帝授予了贝特霍尔德三座位于汝拉山和今天瑞士圣伯纳德大山口（Great St. Bernard Pass）之间的城市，即洛桑、日内瓦和锡永（Sion）。 勃艮第的其他地区则都留给了皇后。奥托记载道，她的领地从巴塞尔以西，贝桑松东北的蒙贝利亚尔（Montbéliard）城堡开始，一直向南延伸到罗讷河（Rhône）的支流伊泽尔河（Isère river）。奥托还记载勃艮第与普罗旺斯相连的地方从伊泽尔河一直延伸到罗讷河口。最后他总结道，在他结婚后，巴巴罗萨“开始以他妻子的名义亲自持有……不仅是勃艮第，而且还有脱离他帝国很久的普罗旺斯。”事实上，贝阿特丽丝并没有奥托这番记载中对上勃艮第地区的全部继承权，更不可能有普罗旺斯的继承权；但奥托的记载可能只是反映了神圣罗马帝国宫廷对勃艮第地区实际情况的无知。

然而，从巴巴罗萨在阿尔萨斯地区的领地出发，穿过贝尔福峡口（Belfort Gap），很容易就能够到达勃艮第，这为他提供了一个取代布伦纳山口的选择，毕竟维罗纳人在1155年将其封锁了，如果从这里走，甚至可以同时从东西两面夹击米兰。从德意志到意大利穿过勃艮第的路线经由流经贝桑松的杜河（Doubs），然后穿过索恩河和罗讷河谷，再经伊泽尔河前往都灵以西的塞尼斯山口和上波河谷。巴巴罗萨于1162年穿过塞尼斯山口离开意大利，1168年也是从这里离开的，他在1174年秋的第五次战役中又从这里进入意大利。 此外，乌尔斯贝格的伯查德（Burchard of Ursberg）在1230年左右记载道，贝阿特丽丝不仅继承了她父亲所有的领地，据说还有5000名骑士作为她的嫁妆。【这也太夸张了吧我寻思】这个数字无疑是严重夸大了，但贝阿特丽丝在1159年巴巴罗萨围攻克雷马期间的确为她的丈夫带来了急需的援兵。在巴巴罗萨于1155年9月从意大利返回时，她所掌握的部队很可能成为了她对巴巴罗萨的主要吸引力。

我们没有关于巴巴罗萨分配给贝阿特丽丝什么土地或收入的信息；但之后帝国的封臣向巴巴罗萨献礼时都被要求再给贝阿特丽丝一份。正如我们之前所看到的，波兰的波列斯瓦夫四世在1157年9月与巴巴罗萨的第一次西里西亚战役结束时，曾答应给他2000金马克，给贝阿特丽丝20。而在1158年米兰向巴巴罗萨投降后，米兰支付给巴巴罗萨的9000马克中她也分到了数量不详的一部分。在1162年米兰第二次投降后，其盟友皮亚琴察同意向皇帝、皇后和廷臣们上贡6000银马克，但却没有说明这笔钱是如何分配的。同年晚些时候，巴巴罗萨将普罗旺斯、福卡尔基耶（Forcalquier）和阿尔勒（Arles）都风给了拉蒙·贝伦加尔三世伯爵（Count Ramón Berengar III）。他欠皇帝12000马克，欠皇后2000马克，欠其他宫廷成员1000马克。最后，巴巴罗萨还与埃诺侯爵鲍德温五世在1184年5月达成协议，将那慕尔（Namur）升格为一个侯爵国。在这些协议达成后，鲍德温必须向巴巴罗萨、他的儿子亨利还有宫廷成员支付80银马克，向皇后支付5金马克。这种上贡的情况有多普遍我们无从得知。

在巴巴罗萨与贝阿特丽丝结婚后，巴巴罗萨需要补偿贝阿特丽丝的堂兄弟——马孔伯爵威廉四世的儿子们对勃艮第省的领土权利。长子斯蒂芬二世参加了贝阿特丽丝的婚礼，得到了位于索恩河东部的欧索讷郡（Auxonne）。他的弟弟杰拉德从斯蒂芬手中夺取了法国索恩河西岸的马孔郡（Mâcon）郡，还有神圣罗马帝国在罗讷河东岸的维埃纳郡（Vienne）郡。这两人都是巴巴罗萨的忠实支持者。斯蒂芬与上洛林公爵马蒂亚斯的女儿洛林的朱迪丝（这个名字和巴巴罗萨的母亲相同）结婚，她既是巴巴罗萨的外甥女，又是贝阿特丽丝的表妹，这加强了这三个家族之间的关系。洛林家族在另一个王室联姻中也占有重要地位。大约在1172年，为了解决波兰-西里西亚继承权争端，朱迪丝的兄弟腓特烈娶了梅什科三世（Mieszko III）的女儿柳德米拉，梅什科三世是波列斯瓦夫四世的弟弟，也是波兰大公爵之位的继承人。

另一个家族关系则更为重要：贝阿特丽丝的外祖父洛林公爵西蒙一世的继兄弟是弗兰德斯伯爵，阿尔萨斯的蒂埃里（Thierrry of Alsace, r. 1128-1168）。他的儿子，弗兰德斯的菲利普伯爵（Count Philip of Flanders, r. 1168-1191）是法国国王的附庸，但积极参与神圣罗马帝国的政治，从血缘上来看她是贝阿特丽丝母亲的表弟。正如巴巴罗萨通过巴本堡家族与帝国东部边境的许多大诸侯有亲戚关系一样，他也通过他的妹妹——上洛林公爵夫人贝莎还有贝阿特丽丝与法国边境几位有权有势的诸侯有关系。

阿塞伯·莫雷纳在1162年描述了贝阿特丽丝，她当时大约20岁左右：

> 她中等身高，有一头金灿灿的头发，长相秀美，有洁白而整齐的牙齿；她的姿势端庄，嘴巴很小，容貌娴静，眼睛闪闪发亮；当有人对奉承她时她会感到害羞。她有一双纤纤玉手和苗条的身材；她对丈夫完全顺从，既将他当做自己的主人一样敬畏，又像对待丈夫那样爱他；她有文化，虔信上帝；正如她的名字叫贝阿特丽丝那样，她是个幸福的人（Beata）。
在伦巴第歌颂巴巴罗萨事迹的无名诗人赞美她“比维纳斯更美，比密涅瓦（Minerva）更聪慧，在权势上又更胜过朱诺（Juno）。除了耶稣的母亲玛利亚之外，再也没有比贝阿特丽丝皇后更杰出的女性。”【这夸得也太肉麻了】这些话中有多少是真心的，有多少只是对宫廷的恭维，我们无从考证。

在1178年9月8日贝阿特丽丝在维埃纳加冕为勃艮第王后的同时，盎格鲁-诺曼人的编年史学家拉尔夫·迪切托（Ralph of Diceto）记载道：“虽然巴巴罗萨不耽于享乐，但很多人认为他是个十分贪婪的人，他在很多事情上都在寻求如何取悦他的妻子。”我们可以从现代婚姻的角度把拉尔夫的这段话理解为巴巴罗萨在恭维他的妻子；但肯定不可能是这个意思。我们不知道拉尔夫是在毫无根据地八卦帝后的私生活，还是巴巴罗萨真的在公开场合也对贝阿特丽丝宠爱有加。亦或者，可以想象，在拉尔夫看来，巴巴罗萨和贝阿特丽丝进行夫妻间亲密活动太过频繁【草，我只能委婉表达一下，你们懂得，毕竟生了那么多个】，这也是拉丁语“uxorius”一词的另一种可能内涵。无论如何，的确没有证据表明巴巴罗萨有任何不忠于他们婚姻的行为。也许他们之间的婚姻真的是这么幸福。
