### 神圣的帝国（5）

由于在1155年参与过巴巴罗萨在罗马加冕事件的人还没有机会进入拉特兰宫，所以在贝桑松的大多数人都不可能亲眼见过这幅画，更不用说在宣读阿德里安的信时联想到这幅画了。雷纳德大概是在1158年1月为巴巴罗萨起草信件时提出了这个问题，以证明他可能自发地将beneficum翻译成了“fief”，然后拉赫温在叙述中才会把发生在贝桑松的事件和壁画联系起来。

其中一位教廷的使节——大概是枢机主教罗兰，在教宗“无礼的致信”所引起骚乱后，又追加了一个问题——拉赫温在描述时使用的措辞表明他也不确定是不是真有这么回事，总之这人问道：“如果不是教宗授予，那么皇帝的帝国是从哪里得到的呢？”从字面上看，这个问题其实很正常，因为也没人否认教宗有为皇帝加冕的权利，但言下之意是，皇帝的权威来自于教宗。愤怒的维特尔斯巴赫的奥托随即拔剑威胁使节——不过拉赫温在记载时也表达了对此事件真实性的怀疑，不过巴巴罗萨还是进行了干预，因为他之前已经许诺保障使节的人身安全。阿德里安随后根据使节团给自己的报告，通告德意志地区的主教们：“皇帝被愤怒冲昏了头，如果要我再复述一遍他是如何对教廷还有使节使用侮辱性的言辞，那也太过分了。”【意思就是实在说不出口】巴巴罗萨派人护送两名枢机主教返回自己的驻地并要求其第二天清晨就离开。他们不被允许在其他主教或是修道院的领地上逗留，必须直接返回罗马。

在两位枢机主教离开贝桑松之前，巴巴罗萨还命令手下搜查他们的住处。在此后他发出的通告中，记载道他们发现了许多阿德里安与他们往来信件的副本和“盖有印章的空白羊皮纸，并且这些羊皮纸可以被随意书写——就像迄今为止他们的所作所为一样，他们正努力将他们的邪恶散布在日耳曼王国的各个教堂中，腐化神职人员，使教会变质……”巴巴罗萨故意迎合地方主教们对教廷勒索的不满。作为对阿德里安腐化主教们的回应，巴巴罗萨禁止任何德意志人访问罗马，据说他还在王国边界派驻了卫兵，拘留任何想要去罗马的人。教宗权利的集中化和托马斯·贝克特（Thomas Becket）与亨利二世的斗争一样，成为冲突的另一个原因。

在巴巴罗萨离开贝桑松之前，他给他的臣民们发表了一份通告，他在通告里宣布了他神圣的权利的来源。上帝委托主的受膏者巴巴罗萨管理王国和帝国，并“规定教会的和平”需要“由帝国的武力来维持。”然而教会的首领以及成为分裂和邪恶的根源，并威胁到教会的团结。通告接下来总结了在贝桑松发生的事情：皇帝在第一天对使节们亲切友好的接待；枢机主教们怀着“傲慢的蔑视”；阿德里安的声明，即他并不后悔赐予巴巴罗萨“maiora beneficia”而不是皇帝的皇冠；皇帝和诸侯们的愤怒——如果不是巴巴罗萨的制止，“两个邪恶的祭祀很可能就会被当场杀死”；并且在枢机主教的行李中发现了作为罪证的信件和空白羊皮纸。巴巴罗萨于是宣称，他“只是从上帝那里，通过诸侯们的选举”获得了王国和帝国。就像尚未加冕的皇帝就会自称皇帝一样，这份宣言也是明确表示了，德意志国王从当选之日其就有资格行使神圣罗马帝国皇帝的皇权。凡是说他是“从教宗冕下那里得到皇冠的人，都违背了神圣的条例‘世人都要受双剑的支配（that the world is subject to dominion by the two swords）’以及圣彼得的教义[敬畏上帝，尊敬国王（Fear God, honor the king）]，而且犯了谎言之罪。”

鉴于巴巴罗萨曾“努力从埃及人【为毛是Egyptians啊，我没太懂，是说他第二次十字军时候的事情吗？但那打的明明是罗姆，和埃及法蒂玛有啥关系……】手中夺取教会的荣誉和自由，教会长期以来一直都受到不应有的压迫”，这大概是指教宗先前对德意志教会事务的干涉，他呼吁德意志地区的主教们的忠诚，不要“允许帝国从城市和基督教的建立一直到你时代的光辉荣誉，被如此低劣的事物所贬谪……”换而言之，皇帝的皇权其实是要早于基督教和教廷的创立。看起来应该是温和的班贝格的埃伯哈德而不是极端分子达塞尔的雷纳德起草了这篇维护帝国自主权的文章。埃伯哈德在通告中没有讨论beneficium究竟该如何翻译。这个词现在到底是“善行”还是“封地”已经完全不重要了，因为皇帝的权利是直接来源于上帝的。

在贝桑松等待巴巴罗萨到来的法国使节很可能是负责安排路易七世和巴巴罗萨之间的会谈。此时，法国国王已经到达了法国边境，贝桑松西部的第戎，等待与德意志君主会面。然而据拉赫温记载，这次会面并没能成行，因为巴巴罗萨已经打道回府了。作为替代，雷纳德和伦茨堡伯爵乌尔里希四世（他作为巴巴罗萨的侍从官曾在第二次十字军中与路易见过）代表皇帝，而路易的宫相则作为国王的使者。表面上这些使节是代表了王室，但拉赫温不屑地表示，他们会面的真正目的，不过是“王室权威的展示”。拉赫温记载道，据特鲁瓦主教亨利（Bishop Henry of Troyes）所说，路易被巴巴罗萨在勃艮第展现的皇权吓坏了，他担心巴巴罗萨“不是来会面的……而是来打仗的。”法国国王随即集结重兵，派遣了九位主教和他们的军队当夜就驻扎在第戎西北部的特鲁瓦。事实上两位君主没能会面的真正原因大概是，路易不想在基督教世界两位最高权威之间的争斗中偏袒任何一方，或者说被别人认为他想要偏袒哪一方。

与此同时枢机主教罗兰和伯纳德已经返回了罗马，据拉赫温记载，他们讲述了在贝桑松发生的事情，进一步地激怒了阿德里安。拉赫温记载，罗马教廷的神职人员内部也出现了一些分歧，有“赞成皇帝并指责是使者犯了无心之过”的人，也有坚定地站在教宗一边的。【emmmm这都能有内鬼的啊？】拉赫温再次重申，他对教宗和皇帝同样尊重，“不能对其中的任何一方草率地做出判断”，所以让他的读者从1157年底教宗发给德意志地区主教的通告中得出自己的结论。

阿德里安以“最深切的悲痛”记录下了这一事件，即巴巴罗萨对教宗使节的接待态度和后续处理方式。值得注意的是，虽然他在第一封信中只是暗示了皇帝的冠冕是他赐予的一种恩惠，但他在后来引用的时候，明确地将其称之为恩惠。“我已将皇冠的恩惠赐予你。（insigne videlicet corone beneficium）”教宗现在觉得可以称得上安慰的是，巴巴罗萨的行动并没有得到多少主教和诸侯们的支持——这也是另一条证据，表明其实没几个德意志地区的主教出现在了贝桑松的会场，因此他相信这些人的意见能够平息他们君主的愤怒。正如巴巴罗萨在通告中提醒主教们要提防罗马教廷的勒索和对德意志地区教会事务的干涉一样，阿德里安也强调，主教们的利益和自由已经受到了巴巴罗萨的威胁。他们不仅有责任将皇帝引回“正道”，而且点名要求雷纳德和维特尔斯巴赫的奥托伯爵为他们令人发指的言论公开道歉，因为他们曾对“使节团和罗马教廷进行了极大的侮辱”。值得注意的是，阿德里安既没有对教宗和皇帝之间的关系作出详细解释，也没有否认雷纳德的翻译。所以其他主教们可以由此得出结论：教宗是真的认为帝国就是教宗的领地，皇帝是他的附庸。
